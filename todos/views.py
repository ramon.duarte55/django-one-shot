from django.shortcuts import render, get_object_or_404
from todos.models import TodoList

# Create your views here.


def todo_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_list_list": todo_lists,
    }
    return render(request, "todos/todo_list.html", context)


def todo_detail(request, id):
    todo_lists = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": todo_lists,
    }
    return render(request, "todos/todo_detail.html", context)
