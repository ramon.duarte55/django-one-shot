from django.urls import path
from todos.views import todo_list, todo_detail


urlpatterns = [
    path("todos/", todo_list, name="todo_list"),
    path("todos/<int:id>/", todo_detail, name="todo_detail"),
]
